require 'rails_helper'

RSpec.describe "ActiveLanguages", type: :request do

  describe "POST :set_active_language" do
    
    it "should set the cookie" do
      language = FactoryBot.create(:language)
      post "/set_active_language/", params: { active_language_id: language.id }
      expect(cookies[:active_language_id]).to_not be_nil
    end

    it "should not set the cookie if language does not exist" do
      language = FactoryBot.create(:language)
      id = language.id + 1
      post "/set_active_language/", params: { active_language_id: id }
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end
end
