require 'rails_helper'

RSpec.describe "Dictionary", type: :request do
  describe "GET /index" do
    it 'renders the index template' do
      get "/dictionary"
      expect(response).to render_template(:index)
    end

    it "renders the parts_of_speech partial" do
      get "/dictionary"
      expect(response).to render_template(partial: "_partsofspeech")
    end

    it "shows the count of parts of speech in the database" do
      5.times { FactoryBot.create(:part_of_speech) }
      get "/dictionary"
      expect(response.body).to include("#{PartOfSpeech.count} parts of speech entries in database")
    end

    it "shows the count of words in the database" do
      5.times { FactoryBot.create(:word) }
      get "/dictionary"
      expect(response.body).to include("#{Word.count} word entries in database")
    end
  end

  describe "GET /show" do
    it 'renders the show template' do
      get "/dictionary/a"
      expect(response).to render_template(:show)
    end

    it "sets @letter instance variable" do
      get "/dictionary/a"
      expect(assigns(:letter)).to be_a(String)
    end

    it "sets @words instance variable" do
      get "/dictionary/a"
      expect(assigns(:words)).to be_a(ActiveRecord::Relation)
    end

    it "renders the definitions partial" do
      FactoryBot.create(:definition)
      get "/dictionary/t"
      expect(response).to render_template(partial: "_definitions")
    end
  end
end
