require 'rails_helper'

RSpec.describe "Words", type: :request do
  describe "GET /index" do

    it "renders the index template" do
      get "/words"
      expect(response).to render_template(:index)
    end
  end

  describe "GET /show/:val" do

    before(:each) do
      @word = FactoryBot.create(:word)
    end

    it "renders the show template" do
      get "/words/#{@word.id}"
      expect(response).to render_template(:show)
    end

    it "sets @word instance variable" do
      get "/words/#{@word.id}"
      expect(assigns(:word)).to be_a(Word)
    end

    it "displays the word name" do
      get "/words/#{@word.id}"
      expect(response.body).to include(@word.word)
    end

    it "displays the definitions" do
      @definition = FactoryBot.create(:definition)
      get "/words/#{@definition.word_id}"
      expect(response.body).to include(@definition.definition)
    end
  end
end
