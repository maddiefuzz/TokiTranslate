require 'rails_helper'

RSpec.describe "Languages", type: :request do

  describe "GET :index" do
    it "returns http success" do
      get "/admin/languages/"
      expect(response).to have_http_status(:success)
    end

    it "lists all languages" do
      language = FactoryBot.create(:language)
      get "/admin/languages/"
      expect(response.body).to include(language.name)
    end

    it "has a new-language link" do
      get "/admin/languages/"
      expect(response.body).to have_selector(%(a[href="#{new_admin_language_path}"]))
    end

    it "has an edit button" do
      language = FactoryBot.create(:language)
      get "/admin/languages/"
      expect(response.body).to have_link("Edit")
    end

    it "has a delete button" do
      language = FactoryBot.create(:language)
      get "/admin/languages/"
      expect(response.body).to have_link("Delete")
    end
  end

  describe "GET :new" do
    it "returns http success" do
      get "/admin/languages/new"
      expect(response).to have_http_status(:success)
    end

    it "has a form for a new language" do
      get "/admin/languages/new"
      expect(response.body).to have_selector(%(input[name="language[name]"]))
    end
  end

  describe "POST :create" do
    it "redirect to admin_languages_path on success" do
      post "/admin/languages", params: { language: { name: "Test Name" } }
      expect(response).to redirect_to(admin_languages_path)
    end

    it "creates a new language" do
      post "/admin/languages", params: { language: { name: "Test Name" } }
      expect(Language.count).to eq(1)
    end

    it "requires a name for a new language" do
      post "/admin/languages", params: { language: { name: nil } }
      expect(response).to have_http_status(:unprocessable_entity)
    end
  end

  describe "GET :edit" do
    before :each do
      @language = FactoryBot.create(:language)
      get "/admin/languages/#{@language.id}/edit"
    end

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "has a form to edit language" do
      expect(response.body).to have_selector(%(input[name="language[name]"]))
    end
  end

  describe "PATCH :update" do
    before :each do
      @language = FactoryBot.create(:language)
      @params = { language: { name: "Test Name Update" } }
    end

    it "updates a language" do
      patch "/admin/languages/#{@language.id}", params: @params
      expect(response).to redirect_to(admin_languages_path)
    end

    it "requires params to update a language" do
      @params = { language: { name: nil, invalid: "oh no" } }
      patch "/admin/languages/#{@language.id}", params: @params
      expect(response).to have_http_status(:unprocessable_entity)
    end

  end

  describe "DELETE :destroy" do
    it "deletes a language" do
      language = FactoryBot.create(:language)
      language2 = FactoryBot.create(:language)
      delete "/admin/languages/#{language.id}"
      expect(Language.all).to eq([language2])
    end
  end
end
