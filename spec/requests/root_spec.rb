require 'rails_helper'

RSpec.describe "Root path", type: :request do
  describe "GET /" do
    it "works! (now write some real specs)" do
      get root_path
      expect(response).to have_http_status(200)
    end

    it "should have a link to the dictionary" do
      get root_path
      expect(response.body).to have_selector(%(a[href="#{dictionary_index_path}"]))
    end
  end

  describe "logged out" do
    it "should have 'Register' link" do
      get root_path
      expect(response.body).to include("Register")
    end
  end

  describe "logged in" do
    it "should have 'Sign Out' link" do
      sign_in FactoryBot.create(:user)
      get root_path
      expect(response.body).to include("Sign Out")
    end

    it "should have the 'Languages' link" do
      sign_in FactoryBot.create(:user)
      get root_path
      expect(response.body).to have_selector(%(a[href="#{admin_languages_path}"]))
    end

    it "should welcome user by username" do
      user = FactoryBot.create(:user)
      sign_in user
      get root_path
      expect(response.body).to include(user.username)
    end

    it "should have a language drop-down" do
      user = FactoryBot.create(:user)
      sign_in user
      get root_path
      expect(response.body).to have_field("active_language_id")
    end
  end
end
