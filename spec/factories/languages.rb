FactoryBot.define do
  factory :language do
    sequence(:name) { |n| "MyString#{n.to_s}" }
  end

  factory :language_english, class: :language do
    name { "English" }
  end
end
