FactoryBot.define do
  factory :word do
    sequence(:word) { |n| "test-#{n.to_s}" }
  end
end
