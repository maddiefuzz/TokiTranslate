FactoryBot.define do
  factory :user do
    sequence(:username) { |n| "test-#{n.to_s.rjust(3, "0")}@sample.com" }
    password { "12345678" }
  end
end
