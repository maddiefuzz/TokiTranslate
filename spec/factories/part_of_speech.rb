FactoryBot.define do
  factory :part_of_speech do
    sequence(:pos) { |n| "test-#{n.to_s}" }
    language_id { FactoryBot.create(:language).id }
    definition { "test definition" }
  end
end
