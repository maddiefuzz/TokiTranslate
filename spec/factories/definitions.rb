FactoryBot.define do
  factory :definition do
    pos { "n" }
    definition { "this is a definition" }
    language_id { FactoryBot.create(:language).id }
    word_id { FactoryBot.create(:word).id }
  end
end
