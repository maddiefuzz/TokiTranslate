require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the ActiveLanguageHelper. For example:
#
# describe ActiveLanguageHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe ActiveLanguageHelper, type: :helper do

  describe "active_language_helper_tag" do
    before :each do
      english = FactoryBot.create(:language_english)
      2.times { FactoryBot.create(:language) }
      @tag = active_language_select_tag
    end

    it "should return a select tag" do
      expect(@tag).to have_selector(%(select))
    end

    it "should not contain an English option" do
      expect(@tag).to_not include("English")
    end

    it "should include css id #active_language_select_tag" do
      expect(@tag).to have_selector(%(select[id="active_language_select_tag"]))
    end
  end
end
