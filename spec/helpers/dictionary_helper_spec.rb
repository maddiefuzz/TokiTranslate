require 'rails_helper'

include Capybara::RSpecMatchers

# Specs in this file have access to a helper object that includes
# the DictionaryHelper. For example:
#
# describe DictionaryHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe DictionaryHelper, type: :helper do

  describe "#alphabetical_links" do

    it "generates fourteen letter-links" do
      expect(helper.alphabetical_links).to have_selector(%(a), count: 14)
    end

    it "generates links for all fourteen letters" do
      output = helper.alphabetical_links
      letters = ['i', 'u', 'e', 'o', 'a', 'm', 'n', 'p', 't', 'k', 's', 'w', 'l', 'j']

      letters.each do |l|
        expect(output).to have_link(l, href: dictionary_path(l))
      end
    end

    it "sorts the toki pona alphabet" do
      expect(DictionaryHelper::LETTERS).to eql(DictionaryHelper::LETTERS.sort)
    end
  end
end
