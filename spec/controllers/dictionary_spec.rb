require 'rails_helper'

RSpec.describe DictionaryController, type: :controller do
  describe "GET :index" do
    it 'renders the index template' do
      get :index
      expect(response).to render_template(:index)
    end
    
    it 'sets @parts_of_speech instance variable' do
      get :index
      FactoryBot.create(:part_of_speech)
      expect(assigns(:parts_of_speech)).to be_a(ActiveRecord::Relation)
    end

    it 'sets @words instance variable' do
      get :index
      FactoryBot.create(:word)
      expect(assigns(:words)).to be_a(ActiveRecord::Relation)
    end
  end
end
