require 'rails_helper'

RSpec.describe WordsController, type: :controller do
  describe "GET :index" do
    it 'renders the index template' do
      get :index
      expect(response).to render_template(:index)
    end

    it 'sets @words instance variable' do
      FactoryBot.create(:word)
      get :index
      expect(assigns(:words)).to be_a(ActiveRecord::Relation)
    end

    it 'fetches all words into @words' do
      5.times { FactoryBot.create(:word) }
      get :index
      expect(assigns(:words).count).to eq(5)
    end
  end
end
