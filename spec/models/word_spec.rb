require 'rails_helper'

RSpec.describe Word, type: :model do
  it "successfully creates a new word" do
    word = FactoryBot.create(:word)
    expect(Word.count).to eq(1)
  end
end
