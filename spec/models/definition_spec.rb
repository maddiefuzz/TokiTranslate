require 'rails_helper'

RSpec.describe Definition, type: :model do
  it "successfully creates a definition" do
    definition = FactoryBot.create(:definition)
    expect(Definition.count).to eq(1)
  end
end
