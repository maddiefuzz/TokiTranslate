# README

TokiTranslate is a community-focused, collaborative website for translating the
[Toki Pona](https://en.wikipedia.org/wiki/Toki_Pona) dictionary into new languages.

Designed from the beginning for community ownership of all translations we produce. Export
of entire translated dictionaries available for download in JSON.

Under active development and not yet live.
