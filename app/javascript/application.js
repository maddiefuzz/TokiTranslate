// Configure your import map in config/importmap.rb. Read more: https://github.com/rails/importmap-rails
import "@hotwired/turbo-rails"
import "controllers"

let switchLanguageForm = document.querySelector("#active_language_form");
let switchLanguageSelectTag = document.querySelector("#active_language_select_tag");

switchLanguageSelectTag.addEventListener("change", (event) => {
  switchLanguageForm.submit();
});
