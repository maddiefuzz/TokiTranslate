module ActiveLanguageHelper

  def active_language_select_tag
    languages = Language.all.to_a.delete_if {|language| language.name == "English" }
    options = options_from_collection_for_select(languages, "id", "name", cookies[:active_language_id] || 1)
    select_tag "active_language_id", options, { id: "active_language_select_tag" }
  end
end
