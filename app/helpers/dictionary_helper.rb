module DictionaryHelper
  LETTERS = ['i', 'u', 'e', 'o', 'a', 'm', 'n', 'p', 't', 'k', 's', 'w', 'l', 'j'].sort

  def alphabetical_links(letter = nil)

    LETTERS.map do |l|
      if l == letter then
          "<p class=\"selected-letter\">#{l.to_s}</p>"
      else
        link_to l.to_s, dictionary_path(l)#, class: "dictionary_index_links text-center"
      end
    end.join("\n")
  end
end
