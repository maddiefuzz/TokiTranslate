class WordsController < ApplicationController

  def index
    @words = Word.all
  end

  def show
    @word = Word.find_by_id(params[:id])
  end
end
