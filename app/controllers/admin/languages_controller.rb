class Admin::LanguagesController < ApplicationController

  def index
    @languages = Language.all
  end

  def new
    @language = Language.new
  end

  def create
    @language = Language.new(language_params)
    if @language.save
      flash[:notice] = "Language successfully created."
      redirect_to admin_languages_path
    else
      render :new, status: :unprocessable_entity
    end
  end

  def edit
    @language = Language.find_by_id(params[:id])
  end

  def update
    @language = Language.find_by_id(params[:id])
    if @language.update(language_params)
      flash[:notice] = "Language successfully updated."
      redirect_to admin_languages_path
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    @language = Language.find_by_id(params[:id])
    if @language.destroy
      flash[:notice] = "Language successfully deleted."
      redirect_to admin_languages_path
    else
      render :index, status: :not_modified
    end
  end

  private
  def language_params 
    params.require(:language).permit(:name)
  end

end
