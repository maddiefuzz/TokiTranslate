class ActiveLanguageController < ApplicationController

  def set_active_language
    if Language.find_by_id(params[:active_language_id]) != nil
      cookies[:active_language_id] = params[:active_language_id]
    else
      redirect_to root_path, status: :unprocessable_entity
    end
  end
end
