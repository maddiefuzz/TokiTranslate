class DictionaryController < ApplicationController
  def index
    @parts_of_speech = PartOfSpeech.all
    @words = Word.all
  end

  def show
    @letter = params[:id]
    @words = Word.where('substr(word, 1, 1) = ?', @letter)
  end
end
