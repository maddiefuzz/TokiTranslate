class Language < ApplicationRecord
  has_many :definitions
  has_many :part_of_speeches

  validates_presence_of :name
end
