Rails.application.routes.draw do
  devise_for :users
  #get 'dictionary/index'
  resources :dictionary, only: [:index, :show, :create]
  resources :words, only: [:index, :show]

  post '/set_active_language', action: :set_active_language, controller: :active_language

  namespace :admin do
    resources :languages
  end
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Reveal health status on /up that returns 200 if the app boots with no exceptions, otherwise 500.
  # Can be used by load balancers and uptime monitors to verify that the app is live.
  get "up" => "rails/health#show", as: :rails_health_check

  # Defines the root path route ("/")
  # root "posts#index"
  root action: :index, controller: :dictionary
end
