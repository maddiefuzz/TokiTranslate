namespace :dataset do
  desc "TODO"
  task ingest: :environment do
    english = create_english_lang
    ingest_pos(english)
    ingest_dictionary(english)
    puts "Ingest complete."
  end

end

def create_english_lang
  if Language.count > 0
    puts "Language English already exists! Skipping step."
    return
  end

  return Language.create(name: "English")
end

def ingest_pos(language)
  if PartOfSpeech.count > 0
    puts "Parts of speech data already exists in table! Aborting."
    return
  end

  parts_of_speech = JSON.parse(File.read('db/dataset_en/toki-partsofspeech.json'))

  parts_of_speech.each do |pos|
    PartOfSpeech.create(pos: pos['pos'], definition: pos['definition'], language_id: language.id)
  end
  puts "Parts of speech ingest complete."
end

def ingest_dictionary(language)
  if Word.count > 0
    puts "Dictionary data already exists in table! Aborting."
    return
  end

  dictionary = JSON.parse(File.read('db/dataset_en/toki-dictionary.json'))

  dictionary.each do |entry|
    if entry['word'] == "a"
      puts entry
    end
    word = Word.create(word: entry['word'])

    entry['definitions'].each do |definition|
      word.definitions.create(pos: definition['pos'], definition: definition['definition'], language_id: language.id)
    end
  end
  puts "Dictionary ingest complete."
end
