class CreateDefinitions < ActiveRecord::Migration[7.1]
  def change
    create_table :definitions do |t|
      t.string :pos
      t.string :definition
      t.references :word, null: false, foreign_key: true

      t.timestamps
    end
  end
end
