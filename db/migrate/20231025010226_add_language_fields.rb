class AddLanguageFields < ActiveRecord::Migration[7.1]
  def change
    create_table :language do |t|
      t.string :name

      t.timestamps
    end

    change_table :definitions do |t|
      t.references :language, null: false, foreign_key: true
    end

    change_table :part_of_speeches do |t|
      t.references :language, null: false, foreign_key: true
    end
  end
end
