class CreatePartOfSpeeches < ActiveRecord::Migration[7.1]
  def change
    create_table :part_of_speeches do |t|
      t.string :pos
      t.string :definition

      t.timestamps
    end
  end
end
